//import connection
const connection = require('./connection')

//import express framework
const express = require('express')

//import body-parser
const bodyParser = require('body-parser');
const { serverHandshake } = require('./connection');

//stored functions in a variable 'app'
var app = express();

//use middleware in app.use ---->bodyparser
//data ->in form of json formate
app.use(bodyParser.json())

//fetch data using get method
//a function req,res -- 
app.get('/employee',(req ,res)=>{
    connection.query('select * from employee',(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log(rows)
            //send on browser
            res.send(rows)
        }
    })
})  

//to fecth details of one employee using id
app.get('/employee/:id',(req,res)=>{
    connection.query('select * from employee where id=?',[req.params.id],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            //show fetched data on console 
            console.log(rows)
            //show fetched data on browser
            res.send(rows)
        }
    })
})


//Delete row from database
app.delete('/employee/:id',(req,res)=>{
    connection.query('delete from employee where id=?',[req.params.id],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log(rows)
            res.send(rows)
        }
    })
})

//insert into database
app.post('/employee',(req,res)=>{
    var emp = req.body
    var empData = [emp.id, emp.name, emp.salary]
    connection.query('INSERT into employee(id,name,salary) values(?)',[empData],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log(rows)
            res.send(rows)
        }
    })
})
app.patch('/employee',(req,res)=>{
    var emp = req.body
    connection.query('update employee set ? where id='+ emp.id,[emp],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
            console.log(rows)
            res.send(rows)
        }
    })
})
app.put('/employee',(req,res)=>{
    var emp = req.body
    connection.query('update employee set ? where id='+ emp.id,[emp],(err,rows)=>{
        if(err){
            console.log(err)
        }
        else{
                if(rows.affectedRows==0){
                    var empData = [emp.id, emp.name, emp.salary]
                    connection.query('INSERT into employee(id,name,salary) values(?)',[empData],(err,rows)=>{
                    if(err){
                        console.log(err)
                    }
                    else{
                        console.log(rows)
                        res.send(rows)
                        }
    })
            }
            else{
                console.log(rows)
                res.send(rows)
            }
        }
    })
})



//set port on which express server will start listening
app.listen(3000,()=>console.log("Express server is running in posrt 3000"))