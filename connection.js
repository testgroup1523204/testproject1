//import mysql
const mysql = require('mysql2'); 

//create connection
var mysqlConnection  = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'manager',
    database:'employeedb'
})

//check whether connected to db or not
    mysqlConnection.connect((err)=>{
    if(err){
        //show error message along with error
        console.log("Error in db connection"+JSON.stringify(err,undefined,2));
    }else{
        console.log("DB connected successfully")
    }
})

module.exports=mysqlConnection
